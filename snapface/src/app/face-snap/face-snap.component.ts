import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';
import { FaceSnapsService } from '../services/face-snaps.services';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements
OnInit{
  @Input() faceSnap!: FaceSnap;
  contentBtnSnaps!: string;

  constructor(private faceSnapsService: FaceSnapsService){}

  ngOnInit() {
    this.contentBtnSnaps = "Oh Snaps";
  }

  onSnap(){
    if(this.contentBtnSnaps === "Oh Snaps"){
      this.faceSnapsService.snapFaceSnapById(this.faceSnap.id, 'snap');
      this.contentBtnSnaps = "Oups Snaps";
    } else{
      this.faceSnapsService.snapFaceSnapById(this.faceSnap.id, 'unsnap');
      this.contentBtnSnaps = "Oh Snaps";
    }

  }

}
